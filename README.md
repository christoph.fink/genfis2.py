# genfis2.py: Generates a Sugeno-Type FIS using subtractive clustering

There is a number of ANFIS implementations in python, such as [github.com/twmeggs/anfis](https://github.com/twmeggs/anfis) and its forks, as well as [github.com/ameybarapatre/ANFIS](https://github.com/ameybarapatre/ANFIS). While these implementations generally are in a beta or even alpha stage of development, they are usable in principle. All of them require to manually specify an input FIS, though. This is the gap this module tries to close, by implementing a `genfis2` algorithm, as suggested by <abbr title="Chiu, S. (1994): Fuzzy Model Identification Based on Cluster Estimation, Journal of Intelligent and Fuzzy Systems 2(3), pp. 267-278">Chiu (1994)</abbr>. 


### References: 
Chiu, S. (1994): Fuzzy Model Identification Based on Cluster Estimation, Journal of Intelligent and Fuzzy Systems 2(3), pp. 267-278
