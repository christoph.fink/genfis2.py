#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__all__ = [
    "subclust"
]


import collections
import math
import numpy


def subclust(
    x,
    radii,
    xBounds=None,
    squashFactor=1.25,
    acceptRatio=0.5,
    rejectRatio=0.15,
    verbose=False
):
    """
        Generates Sugeno-type FIS using substractive clustering

        Args::
            x (iterable of iterables of float)
                    matrix of  FIS input columns.
            radii (float)
                    range of influence of the cluster centres,
                    range [0…1], smaller values result in more rules.
            xBounds (iterable of float)
                    if specified: normalise the xIn and xOut values by
                    the supplied coefficients (one per xIn/xOut column).
                    Default: None
            squashFactor (float):
                    squash factor, used to multiply the `radii` values to
                    determine a minimum cluster distance
                    Default: 1.25
            acceptRatio (float):
                    acceptance ratio, the minimum potential of an n-th cluster
                    centre to be accepted (fraction if (n-1)th cluster centre’s
                    potential)
                    Default: 0.5
            rejectRatio (float):
                    rejectance ratio, analogue to acceptRatio, maximum
                    potential of an n-th cluster centre to be discarded
                    Default: 0.15
            verbose (boolean):
                    print progress information.
                    Default: False

        Returns::
            (dict): {
                        "centers": cluster centers
                        "sigmas":  range of influence
                    }

    """
    x = numpy.array(x, copy=True)

    numPoints, numParams = x.shape

    if not isinstance(radii, collections.Iterable):
        radii = numpy.array(
            [radii for _ in range(numParams)]
        )

    if xBounds is not None:
        xMin = xBounds[0]
        xMax = xBounds[1]
    else:
        xMin = x.min()
        xMax = x.max()

    # distance multipliers for accumulating and squashing cluster potentials
    accumulationMultiplier = 1.0 / radii
    squashMultiplier = 1.0 / (squashFactor * radii)

    # normalise the data
    for i in range(numParams):
        x[:, i] = (x[:, i] - xMin) / (xMax - xMin)

    potVals = numpy.zeros(numPoints)

    for i in range(numPoints):
        thePoint = x[i, ]
        potVals[i] += 1.0

        for j in range(i + 1, numPoints):
            nextPoint = x[j, ]
            dx = (thePoint - nextPoint) * accumulationMultiplier
            dxSq = numpy.dot(dx, dx)
            mu = math.exp(-4.0 * dxSq)
            potVals[i] += mu
            potVals[j] += mu

    maxPotIndex = numpy.argmax(potVals)
    refPotVal = maxPotVal = potVals[maxPotIndex]

    centers = []
    numClusters = 0
    findMore = 1

    while findMore and maxPotVal:
        findMore = 0
        maxPoint = x[maxPotIndex, ]

        maxPotRatio = maxPotVal / refPotVal

        if maxPotRatio > acceptRatio:
            findMore = 1
        elif maxPotRatio > rejectRatio:
            minDistSq = -1

            for i in range(numClusters):
                dx = (maxPoint - centers[i]) * accumulationMultiplier
                dxSq = numpy.dot(dx, dx)
                if minDistSq < 0 or dxSq < minDistSq:
                    minDistSq = dxSq

            minDist = math.sqrt(minDistSq)
            if (maxPotRatio + minDist) >= 1:
                findMore = 1
            else:
                findMore = 2

        if findMore == 1:
            centers.append(maxPoint)
            numClusters += 1

            for i in range(numPoints):
                nextPoint = x[i, ]
                potVal = potVals[i]
                dx = (maxPoint - nextPoint) * squashMultiplier
                dxSq = numpy.dot(dx, dx)

                potVal -= (maxPotVal * math.exp(-4.0 * dxSq))
                if potVal < 0:
                    potVal = 0

                potVals[i] = potVal

                maxPotIndex = numpy.argmax(potVals)
                maxPotVal = potVals[maxPotIndex]

        elif findMore == 2:
            potVals[maxPotIndex] = 0

            maxPotIndex = numpy.argmax(potVals)
            maxPotVal = potVals[maxPotIndex]

    centers = numpy.array(centers)
    for i in range(numParams):
        centers[:, i] = (centers[:, i] * (xMax - xMin)) + xMin

    sigmas = (radii * (xMax - xMin)) / math.sqrt(8.0)

    return((centers, sigmas))


if __name__ == "__main__":
    pass
