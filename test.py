#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy

from matplotlib import (
    pyplot
)

from genfis2 import (
    genfis2
)
from subclust import (
    subclust
)


def main():
    data = {}
    data["x"] = numpy.arange(-10.0, 10.0, 0.2)
    data["y"] = numpy.sin(data["x"]) / data["x"]

    data = numpy.array(
        [*zip(data["x"], data["y"])]
    )

    centers, sigmas = subclust(data, 0.3)

    print(centers)
    print(sigmas)

    pyplot.plot(
        data[:, 0],
        data[:, 1],
        marker='o',
        markersize=4
    )
    pyplot.scatter(
        *zip(*centers),
        marker='x',
        s=128,
        c='red'
    )
    pyplot.show()

    x = genfis2(
        data[:, 0].reshape(100,1),
        data[:, 1].reshape(100,1),
        0.3
    )


if __name__ == "__main__":
    main()
